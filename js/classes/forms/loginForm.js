import {email, password, submitEnter} from "../../configs/index.js";
import {Input} from '../formFields/index.js';
import Form from "./Form.js";
class LoginForm extends Form{
    constructor({className, id, action, func}){
        super({className, id, action, func});
    }
    render() {
        super.render();
        const emailInput = new Input(email);
        const passwordInput = new Input(password);
        const buttonSubmit = new Input(submitEnter);
        this.elem.append(emailInput.render());
        this.elem.append(passwordInput.render());
        this.elem.append(buttonSubmit.render());
        this.elem.addEventListener('submit', this.handleSubmit.bind(this));
        return this.elem;
    }
    async handleSubmit(e){
        e.preventDefault();
        const body = this.serializeJSON();
        const {data} = await axios.post(this.action, body);
        if (data.status === 'Success'){
            localStorage.setItem('token',data.token);
            this.func();
        }
    }
}

export default LoginForm;