import {selectDoctor, submitEnter} from "../../configs/index.js";
import Form from "./Form.js";
import {Input,Select,Textarea} from '../formFields/index.js';
class CreateCardForm extends Form{
    constructor({className, id, action, func}){
    super({className, id, action, func});
    }
    render(){
        super.render();
        const selectDoctors = new Select(selectDoctor);
        const selectDoctorElement = selectDoctors.render();
        this.elem.append(selectDoctorElement);
        selectDoctorElement.addEventListener("change", this.selectDoctor.bind(this));
        this.elem.addEventListener('submit', this.handleSubmit.bind(this));
        return this.elem;
    }
    async handleSubmit(e){
        e.preventDefault();
        const body = this.serializeJSON();
        const {data} = await axios.post(this.action, body);
        if (data.status === 'Success'){
            localStorage.setItem('token',data.token);
            console.log(localStorage.getItem("token"));
            this.func();
        }
    }

    selectDoctor(){

    }
}
export default CreateCardForm;