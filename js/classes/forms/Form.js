import {addProperties, addAttributes} from '../../functions';
class Form {
    constructor({className, id, action, func}){
        const data = {className, id, action, func};
        addProperties(this, data);
        this.elem = null;
    }
    render(){
        this.elem = document.createElement('form');
        const {elem,func, ...attributes} = this;
        addAttributes(this.elem, attributes);
        return this.elem;
    }

    serializeJSON(){
        const body = {};
        const fields = [...this.elem.querySelectorAll('input[name], select[name], textarea[name]')];
        const notEmptyFields = fields.filter(({value}) => value);
        notEmptyFields.forEach(({name, value}) => body[name] = value);

        return body;
    }
}
export default Form;