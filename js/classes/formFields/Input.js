import {addProperties, addAttributes} from '../../functions';
class Input {
    constructor({type, className, id, name, placeholder, required = false, value = ''}){
        const data = {type, className, id, name, placeholder, required, value};
        addProperties(this, data);
        this.elem = null;
    }
    render(){
        this.elem = document.createElement('input');
        const {elem, ...attributes} = this;
        addAttributes(this.elem, attributes);
        return this.elem;
    }
}

export default Input;