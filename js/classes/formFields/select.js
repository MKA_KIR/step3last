import {addProperties, addAttributes} from '../../functions';
class Select {
    constructor({className, id, name, options}){
        const data = { className, id, name, options};
        addProperties(this, data);
        this.elem = null;
    }
    render(){
        this.elem = document.createElement('select');
        const {elem,options, ...attributes} = this;
        addAttributes(this.elem, attributes);
        const optionsHTML = this.options.map(({value,text}) => `<option value="${value}">${text}</option>`).join();
        this.elem.insertAdjacentHTML('beforeend', optionsHTML);
        return this.elem;
    }
}
export default Select;