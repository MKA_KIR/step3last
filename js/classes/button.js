import {addAttributes, addProperties} from "../functions";

class Button {
    constructor({className, id, text, func}){
        const data = {className, id, text, func};
        addProperties(this, data);
        this.elem = null;
    }
    render(){
        this.elem = document.createElement('button');
        const {elem,func,text, ...attributes} = this;
        addAttributes(this.elem, attributes);
        this.elem.textContent = this.text;
        this.elem.addEventListener('click', this.func);
        return this.elem;
    }
}

export default Button;