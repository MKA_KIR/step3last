import Modal from './Modal.js'
import LoginForm from '../forms/loginForm.js'
import login from "../../configs/forms/login.js"
class LoginModal extends Modal{
    constructor({className, id, openClass, func}){
   super({className, id, openClass});
   this.func = func;
    }
    render() {
        super.render();
        login.func = this.func;
        const loginForm = new LoginForm(login);
        this.elem.append(loginForm.render());
        return this.elem
    }
}
export default LoginModal;