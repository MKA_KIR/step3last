class Modal {
    constructor({className, id, openClass}){
        this.className = className;
        this.id = id;
        this.openClass = openClass;
        this.elem = null;
    }
    render(){
        this.elem = document.createElement('div');
        this.elem.className = this.className;
        this.elem.id = this.id;
        this.elem.insertAdjacentHTML('beforeend', `<span class="modal-close">X</span><div class="modal-body"></div>`);
        const closeButton = this.elem.querySelector('.modal-close');
        closeButton.addEventListener('click', this.close.bind(this));
    }
    open(){
        this.elem.classList.add(this.openClass);
    }
    close(){
        this.elem.classList.remove(this.openClass);
    }
}
export default Modal;