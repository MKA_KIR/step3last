import Modal from './Modal.js'
import {CreateCardForm} from '../forms/index.js'
import {createCardProps} from "../../configs/index.js";

class CreateCardModal extends Modal{
    constructor({className, id, openClass, func}){
        super({className, id, openClass});
        this.func = func;
    }
    render(){
        super.render();
        createCard.func = this.func;
        const createCardForm = new CreateCardForm(createCardProps);
        this.elem.append(createCardForm.render());
        return this.elem
    }
}
export default CreateCardModal;