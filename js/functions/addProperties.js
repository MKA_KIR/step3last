function addProperties(obj, data) {
    for(const [key, value] of Object.entries(data)) {
        if(typeof value === "object" && value !== null && !Array.isArray(value)) {
            obj[key] = null;
            addProperties(obj[key], value);
        } else if(Array.isArray(value)) {
            obj[key] = [...value];
        }
        else {
            obj[key] = value;
        }
    }
}
export default addProperties;