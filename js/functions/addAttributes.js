function addAttributes (elem, attributes) {
    for(const [key,value] of Object.entries(attributes)){
        if(value) {
            elem[key] = value;
        }
    }
}
export default addAttributes;