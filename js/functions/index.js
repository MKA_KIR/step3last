import addProperties from "./addProperties.js";
import addAttributes from './addAttributes.js';
import replaceElement from './replaceElement.js'
export {addProperties, addAttributes, replaceElement};
