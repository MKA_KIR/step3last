function replaceElement(oldElem, newElem) {
    oldElem.after(newElem);
    oldElem.remove();
}

export default replaceElement;