const createCard = {
    className:'modal',
    id: 'create-card-modal',
    openClass: 'show'
};
export {createCard};