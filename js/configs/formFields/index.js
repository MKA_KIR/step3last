export {email} from "./email.js";
export {password} from "./password.js";
export {selectDoctor} from "./selectDoctor.js";
export {submitEnter} from "./submitEnter.js";