const selectDoctor = {
    className: 'form-control',
    id: 'select-doctor-type',
    name: 'doctor-type',
    options: [
        {
            value: "",
            text: "Выберите доктора"
        },
        {value: 'VisitDentist', text: 'Стоматолог'},
        {value: 'VisitCardiologist', text: 'Кардиолог'},
        {value: 'VisitTherapist', text: 'Терапевт'}
        ]
};
export {selectDoctor};