const password = {
    type: 'password',
    className: 'form-control',
    id: 'user-password',
    name: 'password',
    placeholder: 'Введите ваш password',
    required: true,
    value: ''
};
export {password};