const email = {
    type: 'email',
    className: 'form-control',
    id: 'user-email',
    name: 'email',
    placeholder: 'Введите ваш email',
    required: true,
    value: ''
};
export {email};